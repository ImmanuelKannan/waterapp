//
//  MainViewController.h
//  Water App
//
//  Created by Immanuel Kannan on 15/08/2016.
//  Copyright © 2016 Immanuel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JTCalendar/JTCalendar.h>

@interface MainViewController : UIViewController < JTCalendarDelegate, UIGestureRecognizerDelegate >

@end
